
const Services = require('../Services');
const { internalError } = require('../settings');

const { queueView, queueCreate, queueDelete, queueUpdate, queueFindOne } = require('./index')

async function  View (job, done){

    try {

        const { } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.View({});

        done(null, { statusCode,data: data.map(v => ({...v.toJSON(), ...{ worker: job.id}})),message });
        
    } catch (error) {
        console.log({step:'adapter queueView',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Create (job, done){

    try {

        const { age, color, name } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Create({age, color, name});

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueCreate',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Delete(job, done){

    try {

        const { id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Delete({ id });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueDelete',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function FindOne(job, done){

    try {

        const { id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.FindOne({id});

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueFindOne',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Update(job, done){

    try {

        const { age, color, name, id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Update({age, color, name, id});

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueUpdate',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function run(){
    try {

        console.log("VAmos a inicializar worker");

        queueView.process(View);

        queueCreate.process(Create);

        queueDelete.process(Delete);

        queueFindOne.process(FindOne);

        queueUpdate.process(Update);

    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    View, Create, Delete, Update, FindOne, run
}