const bull = require('bull');

const redis = {
    host: 'localhost',
    port: 6379,
}

const opts = { redis: {host: redis.host , port: redis.port }}

const queueCreate = bull('curso:create', opts)
const queueDelete = bull('curso:delete', opts)
const queueUpdate = bull('curso:update', opts)
const queueFindOne = bull('curso:findOne', opts)
const queueView = bull('curso:view', opts)


async function Create({age, color, name }){
    try {
        const job = await queueCreate.add({age, color, name })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Delete({ id }){
    try {
        const job = await queueDelete.add({ id })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function FindOne({id}){
    try {
        const job = await queueFindOne.add({id})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Update({ age, color, name, id }){
    try {
        const job = await queueUpdate.add({age, color, name, id})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function View({}){
    try {
        const job = await queueView.add({})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function main() {

    //await Create({name: 'Maria', age:27, color: 'Rojo'})

    //await Delete({ id: 2})

    //await Update({age:30, id:1})

    await FindOne({id:1})

    //await View({});
}

//main()

module.exports = { Create, Delete, Update, FindOne, View }

